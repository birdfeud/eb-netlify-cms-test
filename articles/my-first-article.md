---
title: My First Article
image: https://source.unsplash.com/random/1
abstract: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
author: Mario Rossi
date: 2020-01-05
---
Sed iaculis hendrerit mauris vel faucibus. Quisque non tortor id tortor rutrum tempus suscipit quis mi. Curabitur tristique purus eget nulla posuere, in sodales ipsum imperdiet. Suspendisse nec est eu dui volutpat imperdiet sit amet eu sapien. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam sed leo volutpat, commodo tellus tincidunt, ullamcorper diam. Etiam vel nisi maximus, maximus justo a, ultricies orci. Interdum et malesuada fames ac ante ipsum primis in faucibus. Etiam convallis tortor sit amet eleifend efficitur. Vivamus facilisis purus laoreet, imperdiet velit ut, efficitur neque. Etiam maximus lacus ut metus lobortis, pellentesque malesuada massa sagittis. Maecenas laoreet massa elementum sem malesuada, eget mollis sem porttitor. Pellentesque urna sem, fermentum et dapibus sed, malesuada ac massa.